module Handler.Tea where

import Import
import Data.Maybe

getTeaR :: TeaId -> Handler RepHtml
getTeaR teaId = do
    maid <- maybeAuth
    (maybeTea, ranks, voteable) <- runDB $ do
        t  <- get teaId
        rs <- selectList [RankTea ==. teaId] []
        rrs <- mapM (\(Entity _ r) -> do
            u <- get404 $ rankUser r
            return (r, u)) rs
        voteable <- case maid of
            Nothing -> return False
            Just u -> do
                myRank <- getBy $ UniqueRank teaId (entityKey u)
                return $ isNothing myRank
        return (t, rrs, voteable)
    defaultLayout $(widgetFile "tea")

postTeaCreateR :: Handler RepHtml
postTeaCreateR = do
    ((result, widget), enctype) <- runFormPost teaForm
    case result of
        FormSuccess tea -> do
            _ <- runDB $ insert tea
            setMessage "Created a new tea successfully." 
            redirect RootR
        _ -> defaultLayout [whamlet|
<p>Invalid input, let's try again.
<form method=post action=@{TeaCreateR} enctype=#{enctype}>
    ^{widget}
    <input type=submit>
|]

teaForm :: Html -> MForm TeaRank TeaRank (FormResult Tea, Widget)
teaForm = renderDivs $ Tea
    <$> areq intField "Number" Nothing
    <*> areq textField "Name" Nothing
    <*> areq textareaField "Description" Nothing
    <*> pure (-1)

