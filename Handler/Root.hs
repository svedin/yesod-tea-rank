module Handler.Root where

import Import
import Yesod.Static

import Handler.Tea
import Data.Maybe

data TeaVotable = TV TeaId Tea Bool

-- This is a handler function for the GET request method on the RootR
-- resource pattern. All of your resource patterns are defined in
-- config/routes
--
-- The majority of the code you will write in Yesod lives in these handler
-- functions. You can spread them across multiple files if you are so
-- inclined, or create a single monolithic file.
getRootR :: Handler RepHtml
getRootR = do
    -- Generate the form to be displayed
    maid <- maybeAuth
    tvs <- runDB $ do
        allTea <- selectList [] [Asc TeaNumber]
        tv <- case maid of
            Nothing -> mapM (\(Entity tId t) -> return $ TV tId t False) allTea
            Just u  -> do
                mapM (\(Entity tId t) -> do
                    mRank <- getBy $ UniqueRank tId (entityKey u)
                    return $ TV tId t (isNothing mRank)) allTea
        return tv
    (widget, enctype) <- generateFormPost teaForm
    defaultLayout $ do
        addScript $ StaticR $ StaticRoute ["js", "jquery-1.7.2.min.js"] []
        addScript $ StaticR $ StaticRoute ["js", "jquery.tablesorter.min.js"] []
        $(widgetFile "root")
