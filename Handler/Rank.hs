module Handler.Rank where

import Import
import Yesod.Auth
import Data.Text (pack)

getRankR :: TeaId -> Handler RepHtml
getRankR teaId = do
    (widget, enctype) <- generateFormPost $ rankForm teaId
    defaultLayout [whamlet|
<form method=post action=@{RankR teaId} enctype=#{enctype}>
    ^{widget}
    <input type=submit>
|]

postRankR :: TeaId -> Handler RepHtml
postRankR teaId = do
    ((result, widget), enctype) <- runFormPost $ rankForm teaId
    case result of
        FormSuccess fRank -> do
            _ <- runDB $ do
                _ <- insert fRank
                ranks <- selectList [RankTea ==. teaId] []
                scores <- mapM (\(Entity _ r) -> return $ rankScore r) ranks
                scoresAverage <- return $ sum scores `div` length scores
                update teaId [TeaAverageScore =. scoresAverage]
            setMessage "Cast a new vote successfully."
            redirect (TeaR teaId)
        _ -> defaultLayout [whamlet|
<p>Invalid input, let's try again.
<form method=post action=@{RankR teaId} enctype=#{enctype}>
    ^{widget}
    <input type=submit>
|]

rankForm :: TeaId -> Form Rank 
rankForm teaId = renderDivs $ Rank
    <$> pure teaId
    <*> aformM requireAuthId
    <*> areq (selectFieldList possibleScores) "Score" Nothing
    <*> areq textField "Message" Nothing
  where
    possibleScores :: [(Text, Int)]
    possibleScores = map (\s -> (pack $ show s, s)) [0..10]
