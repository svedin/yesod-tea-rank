module Handler.User where

import Import
import Yesod.Auth

getUserR :: Handler RepHtml
getUserR = do
    userId <- requireAuthId
    user <- runDB $ get404 userId
    (widget, enctype) <- generateFormPost $ userForm user
    defaultLayout [whamlet|
<form method=post action=@{UserR} enctype=#{enctype}>
    ^{widget}
    <input type=submit>
|]

postUserR :: Handler RepHtml
postUserR = do
    userId <- requireAuthId
    user <- runDB $ get404 userId
    ((result, widget), enctype) <- runFormPost $ userForm user 
    case result of
        FormSuccess (User _ _ name) -> do
            _ <- runDB $ update userId [UserName =. name]
            setMessage "Updated profile succesfully."
            redirect RootR
        _ -> defaultLayout [whamlet|
<p>Invalid input, let's try again.
<form method=post action=@{UserR} enctype=#{enctype}>
    ^{widget}
    <input type=submit>
|]

userForm :: User -> Form User
userForm user = renderDivs $ User
    <$> pure (userIdent user)
    <*> pure (userPassword user)
    <*> areq textField "Username" (Just $ userName user)
